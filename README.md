# Beres, the worst RSS feed reader you've ever seen

Beres is a simple Bash script that queries all RSS feeds in a file (`$HOME/.beres/urls`) and fetches everything that happened yesterday and formats every entry into a separate Markdown file. It doesn't care what you do with the text files after reading them. Its only function is to pull entries. It only runs when explicitly ran from the command line. By only pulling things that happened yesterday, Beres doesn't have to keep "state" of when it was last ran, at the cost of missing articles if you don't fetch them for a few days or so.

I have made a great effort to reduce the number of dependencies and only use ones that don't require additional system libraries to be as friendly as possible to embedded Linux systems (for example, my Kobo Libra H2O).

## Dependencies

- Bash, or a similar shell. `sh` may or may not work depending on implementation (see known issues below)
- [bat](https://github.com/astaxie/bat)
- [dasel](https://github.com/TomWright/dasel) (file to compile is `cmd/dasel/main.go`)
- [html2md](https://github.com/suntong/html2md) (`go install github.com/suntong/html2md@latest`)
- `tr` & `sed`
- GNU coreutils `date` (Busybox will NOT work. If on an embedded device like Kobo, steal a `date` binary from a Debian `armhf` chroot and rename it `date2` to avoid conflicts / potential bricking and then put it in your `$PATH`)

## Installation

1. `git clone` this repo
2. install all dependencies to somewhere in your $PATH
3. if GNU coreutils `date` is already installed, symlink it to `date2` somewhere in your `$PATH`
4. symlink `beres` anywhere in your $PATH
5. put URLs in `$HOME/.beres/urls`
6. run `beres` once a day to fetch your articles :) the articles will be in `$HOME/.beres/dump/`

## Known issues

- On Kobo, `sh` will complain about "not found" if `beres` is called directly. This is because the shebang says to run the script with Bash, but Kobo doesn't have Bash installed. Run it with `sh /path/to/beres`. Changing the shebang to `#!/bin/sh` is apparently *not* an option due to some quirk somewhere in `sh`.
- Atom-style feeds are currently not supported.
- Feeds without titles or that have special characters will produce errors on FAT filesystems.
- `dasel` freaks out if there's only one entry in an RSS feed.

## License

GPLv3 only.
